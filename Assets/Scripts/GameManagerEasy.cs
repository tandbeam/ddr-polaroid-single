﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class GameManagerEasy : MonoBehaviour {
    public Transform left;
    public Transform down;
    public Transform up;
    public Transform right;
    public static int score;
    public Text scoreText;
    public static int beat;
    public Text beatText;

    private float timeStep;

    private bool[] leftNotes = new bool[550];
    private bool[] downNotes = new bool[550];
    private bool[] upNotes = new bool[550];
    private bool[] rightNotes = new bool[550];

    void Awake ()
    {
        beat = 0;
        timeStep = 0;
        score = 0;
        scoreText.text = "Score: " + score.ToString();
        beatText.text = "";

        //Intro
        leftNotes[0] = true;
        downNotes[8] = true;
        rightNotes[17] = true;
        upNotes[25] = true;

        //Walking by the ribbon
        leftNotes[33] = true;
        rightNotes[37] = true;
        leftNotes[39] = true;
        rightNotes[41] = true;

        //Luck is on my side
        upNotes[49] = true;
        downNotes[51] = true;
        upNotes[53] = true;
        downNotes[55] = true;
        upNotes[57] = true;

        //Raindrops falling on me
        rightNotes[65] = true;
        leftNotes[69] = true;
        rightNotes[73] = true;
        leftNotes[75] = true;

        //I have no umbrella
        downNotes[81] = true;
        upNotes[82] = true;
        downNotes[83] = true;
        upNotes[84] = true;

        //Search in my car
        leftNotes[96] = true;
        leftNotes[98] = true;
        rightNotes[100] = true;

        //Search in my house
        rightNotes[103] = true;
        rightNotes[105] = true;
        leftNotes[107] = true;

        //I can't find just anything I want
        upNotes[110] = true;
        downNotes[112] = true;
        upNotes[114] = true;
        downNotes[116] = true;
        leftNotes[118] = true;
        rightNotes[120] = true;
        leftNotes[122] = true;

        //Search in my heart
        upNotes[125] = true;
        upNotes[127] = true;
        downNotes[128] = true;

        //Search in hopes
        downNotes[132] = true;
        downNotes[134] = true;
        upNotes[136] = true;

        //There's no answer
        leftNotes[139] = true;
        rightNotes[141] = true;
        leftNotes[143] = true;
        rightNotes[145] = true;

        //Any time, any place
        upNotes[147] = true;
        upNotes[149] = true;
        downNotes[150] = true;
        downNotes[152] = true;

        //(boop beep, boop beep)
        leftNotes[154] = true;
        downNotes[165] = true;
        upNotes[173] = true;
        rightNotes[178] = true;
        downNotes[181] = true;

        //Walking by the ribbon
        leftNotes[183] = true;
        leftNotes[187] = true;
        rightNotes[189] = true;
        rightNotes[191] = true;

        //Luck is by my side
        upNotes[197] = true;
        upNotes[199] = true;
        downNotes[201] = true;
        downNotes[203] = true;
        upNotes[205] = true;

        //Raindrops falling on me
        rightNotes[212] = true;
        rightNotes[215] = true;
        leftNotes[219] = true;
        leftNotes[221] = true;

        //I have no umbrella
        downNotes[226] = true;
        upNotes[228] = true;
        upNotes[229] = true;
        downNotes[230] = true;

        //Talking to my boyfriend
        leftNotes[240] = true;
        rightNotes[243] = true;
        rightNotes[245] = true;
        leftNotes[247] = true;

        //Such a happy time
        upNotes[254] = true;
        upNotes[256] = true;
        downNotes[258] = true;
        downNotes[260] = true;
        upNotes[262] = true;

        //He'll say goodnight to me
        rightNotes[268] = true;
        rightNotes[270] = true;
        leftNotes[272] = true;
        leftNotes[275] = true;
        rightNotes[277] = true;

        //I feel all alone
        downNotes[280] = true;
        upNotes[282] = true;
        upNotes[284] = true;
        downNotes[286] = true;

        //Ready for you
        leftNotes[295] = true;
        leftNotes[297] = true;
        rightNotes[299] = true;

        //Ready for me
        rightNotes[302] = true;
        rightNotes[304] = true;
        leftNotes[306] = true;

        //I don't know what I'm just looking for
        leftNotes[309] = true;
        downNotes[310] = true;
        upNotes[312] = true;
        rightNotes[314] = true;
        upNotes[315] = true;
        upNotes[317] = true;
        upNotes[319] = true;

        //Ready for birth
        leftNotes[322] = true;
        leftNotes[324] = true;
        rightNotes[325] = true;

        //Ready for death
        rightNotes[328] = true;
        rightNotes[330] = true;
        leftNotes[331] = true;

        //There's no answer
        leftNotes[335] = true;
        upNotes[336] = true;
        rightNotes[338] = true;
        downNotes[339] = true;

        //Any time, any place
        leftNotes[341] = true;
        leftNotes[343] = true;
        rightNotes[344] = true;
        rightNotes[346] = true;

        //(Dun Duun, Don Doon)
        rightNotes[348] = true;
        upNotes[360] = true;
        downNotes[370] = true;
        leftNotes[377] = true;
        upNotes[379] = true;
        upNotes[380] = true;
        downNotes[381] = true;
        downNotes[383] = true;

        //Looking for my answer
        leftNotes[387] = true;
        upNotes[389] = true;
        downNotes[390] = true;
        leftNotes[392] = true;

        //Looking for my dreams
        rightNotes[398] = true;
        upNotes[400] = true;
        downNotes[401] = true;
        rightNotes[402] = true;

        //Starting over
        upNotes[408] = true;
        downNotes[416] = true;

        //Looking for my answer
        leftNotes[428] = true;
        upNotes[430] = true;
        downNotes[431] = true;
        leftNotes[432] = true;

        //Looking for my future
        rightNotes[437] = true;
        upNotes[439] = true;
        downNotes[440] = true;
        rightNotes[442] = true;

        //Hey I got to watch I know
        upNotes[446] = true;
        downNotes[448] = true;
        upNotes[450] = true;
        downNotes[452] = true;
        leftNotes[454] = true;
        rightNotes[456] = true;
        rightNotes[458] = true;

        //Step by step
        leftNotes[462] = true;
        leftNotes[464] = true;

        //Hop through mountains
        upNotes[466] = true;
        upNotes[468] = true;

        //Step by step
        rightNotes[470] = true;
        rightNotes[472] = true;

        //Hop through oceans
        upNotes[473] = true;
        upNotes[475] = true;

        //Step by step
        leftNotes[477] = true;
        leftNotes[479] = true;

        //Hop forever
        upNotes[480] = true;
        upNotes[481] = true;

        //Going my way
        leftNotes[484] = true;
        leftNotes[486] = true;
        rightNotes[488] = true;
        rightNotes[489] = true;

        //Jumping down
        upNotes[491] = true;
        downNotes[493] = true;

        //Hop the mountains
        leftNotes[495] = true;
        leftNotes[497] = true;

        //Jumping down
        upNotes[498] = true;
        downNotes[500] = true;

        //Hop the oceans
        rightNotes[501] = true;
        rightNotes[503] = true;

        //Jumping down
        upNotes[505] = true;
        downNotes[507] = true;

        //Hop forever
        leftNotes[508] = true;
        rightNotes[509] = true;

        //I can guess everything
        leftNotes[510] = true;
        leftNotes[512] = true;
        rightNotes[513] = true;
        rightNotes[515] = true;
    }

    void Start ()
    {
        AudioSource audio = GetComponent<AudioSource>();
        audio.PlayDelayed(4.0f);
    }

    void FixedUpdate ()
    {
        timeStep += Time.deltaTime;
        if (timeStep > 0.295f && beat < 549)
        {
            scoreText.text = "Score: " + score.ToString();
            //beatText.text = "Beat: " + (beat - 15).ToString();

            if (leftNotes[beat] == true)
            {
                Instantiate(left, new Vector3(-2.25f, -6, 0), Quaternion.identity);
            }
            if (downNotes[beat] == true)
            {
                Instantiate(down, new Vector3(-0.75f, -6, 0), Quaternion.identity);
            }
            if (upNotes[beat] == true)
            {
                Instantiate(up, new Vector3(0.75f, -6, 0), Quaternion.identity);
            }
            if (rightNotes[beat] == true)
            {
                Instantiate(right, new Vector3(2.25f, -6, 0), Quaternion.identity);
            }
            timeStep = 0.0f;
            beat += 1;
        }
    }
}
