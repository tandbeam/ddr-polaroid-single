﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class GameManagerHard : MonoBehaviour {
    public Transform left;
    public Transform down;
    public Transform up;
    public Transform right;
    public static int score;
    public Text scoreText;
    public static int beat;
    public Text beatText;

    private float timeStep;

    private bool[] leftNotes = new bool[550];
    private bool[] downNotes = new bool[550];
    private bool[] upNotes = new bool[550];
    private bool[] rightNotes = new bool[550];

    void Awake ()
    {
        beat = 0;
        timeStep = 0;
        score = 0;
        scoreText.text = "Score: " + score.ToString();
        beatText.text = "";

        //Intro
        leftNotes[0] = true;
        upNotes[0] = true;
        downNotes[8] = true;
        rightNotes[8] = true;
        rightNotes[17] = true;
        leftNotes[17] = true;
        upNotes[25] = true;
        downNotes[25] = true;

        //Walking by the ribbon
        leftNotes[33] = true;
        leftNotes[35] = true;
        rightNotes[37] = true;
        rightNotes[39] = true;
        upNotes[41] = true;
        leftNotes[43] = true;
        rightNotes[43] = true;
        downNotes[43] = true;

        //Luck is on my side
        upNotes[49] = true;
        upNotes[51] = true;
        rightNotes[53] = true;
        rightNotes[55] = true;
        downNotes[57] = true;
        upNotes[57] = true;

        //Raindrops falling on me
        rightNotes[65] = true;
        rightNotes[67] = true;
        leftNotes[69] = true;
        leftNotes[71] = true;
        downNotes[73] = true;
        leftNotes[75] = true;
        rightNotes[75] = true;
        upNotes[75] = true;

        //I have no umbrella
        upNotes[81] = true;
        leftNotes[82] = true;
        leftNotes[83] = true;
        downNotes[84] = true;
        downNotes[86] = true;
        upNotes[88] = true;
        downNotes[88] = true;

        //Search in my car
        leftNotes[96] = true;
        leftNotes[97] = true;
        leftNotes[98] = true;
        rightNotes[100] = true;
        downNotes[100] = true;
        upNotes[100] = true;

        //Search in my house
        rightNotes[103] = true;
        rightNotes[104] = true;
        rightNotes[105] = true;
        leftNotes[107] = true;
        upNotes[107] = true;
        downNotes[107] = true;

        //I can't find just anything I want
        upNotes[110] = true;
        downNotes[112] = true;
        upNotes[114] = true;
        downNotes[116] = true;
        leftNotes[118] = true;
        rightNotes[119] = true;
        leftNotes[120] = true;
        rightNotes[121] = true;
        leftNotes[122] = true;
        rightNotes[122] = true;

        //Search in my heart
        upNotes[125] = true;
        upNotes[126] = true;
        upNotes[127] = true;
        downNotes[128] = true;
        leftNotes[128] = true;
        rightNotes[128] = true;

        //Search in hopes
        downNotes[132] = true;
        downNotes[133] = true;
        downNotes[134] = true;
        upNotes[136] = true;
        rightNotes[136] = true;
        leftNotes[136] = true;

        //There's no answer
        leftNotes[139] = true;
        rightNotes[141] = true;
        leftNotes[143] = true;
        rightNotes[145] = true;

        //Any time, any place
        upNotes[147] = true;
        upNotes[148] = true;
        upNotes[149] = true;
        leftNotes[149] = true;
        downNotes[150] = true;
        downNotes[151] = true;
        downNotes[152] = true;
        rightNotes[152] = true;

        //(boop beep, boop beep)
        leftNotes[154] = true;
        downNotes[157] = true;
        upNotes[160] = true;
        rightNotes[163] = true;
        leftNotes[165] = true;
        downNotes[167] = true;
        upNotes[169] = true;
        rightNotes[171] = true;
        leftNotes[173] = true;
        downNotes[175] = true;
        upNotes[176] = true;
        rightNotes[177] = true;
        leftNotes[178] = true;
        downNotes[179] = true;
        upNotes[180] = true;
        rightNotes[181] = true;

        //Walking by the ribbon
        leftNotes[183] = true;
        leftNotes[185] = true;
        rightNotes[187] = true;
        rightNotes[189] = true;
        upNotes[190] = true;
        leftNotes[192] = true;
        rightNotes[192] = true;
        upNotes[192] = true;

        //Luck is by my side
        upNotes[197] = true;
        upNotes[199] = true;
        rightNotes[201] = true;
        rightNotes[203] = true;
        downNotes[205] = true;
        upNotes[205] = true;

        //Raindrops falling on me
        rightNotes[212] = true;
        rightNotes[214] = true;
        leftNotes[215] = true;
        leftNotes[217] = true;
        downNotes[219] = true;
        leftNotes[221] = true;
        rightNotes[221] = true;
        downNotes[221] = true;

        //I have no umbrella
        upNotes[226] = true;
        leftNotes[228] = true;
        leftNotes[229] = true;
        downNotes[230] = true;
        downNotes[232] = true;
        leftNotes[234] = true;
        rightNotes[234] = true;

        //Talking to my boyfriend
        leftNotes[240] = true;
        leftNotes[242] = true;
        rightNotes[243] = true;
        rightNotes[245] = true;
        upNotes[247] = true;
        leftNotes[249] = true;
        rightNotes[249] = true;
        downNotes[249] = true;

        //Such a happy time
        upNotes[254] = true;
        upNotes[256] = true;
        rightNotes[258] = true;
        rightNotes[260] = true;
        downNotes[262] = true;
        upNotes[262] = true;

        //He'll say goodnight to me
        rightNotes[268] = true;
        rightNotes[270] = true;
        leftNotes[271] = true;
        leftNotes[273] = true;
        downNotes[275] = true;
        rightNotes[276] = true;
        leftNotes[276] = true;
        upNotes[276] = true;

        //I feel all alone
        upNotes[281] = true;
        leftNotes[282] = true;
        leftNotes[284] = true;
        downNotes[286] = true;
        rightNotes[288] = true;
        leftNotes[288] = true;

        //Ready for you
        leftNotes[295] = true;
        leftNotes[296] = true;
        leftNotes[297] = true;
        rightNotes[299] = true;
        upNotes[299] = true;
        downNotes[299] = true;

        //Ready for me
        rightNotes[302] = true;
        rightNotes[303] = true;
        rightNotes[304] = true;
        leftNotes[305] = true;
        upNotes[305] = true;
        downNotes[305] = true;

        //I don't know what I'm just looking for
        leftNotes[309] = true;
        downNotes[310] = true;
        upNotes[312] = true;
        rightNotes[314] = true;
        upNotes[315] = true;
        downNotes[316] = true;
        upNotes[317] = true;
        downNotes[318] = true;
        rightNotes[319] = true;
        leftNotes[319] = true;

        //Ready for birth
        leftNotes[322] = true;
        leftNotes[323] = true;
        leftNotes[324] = true;
        rightNotes[325] = true;
        upNotes[325] = true;
        downNotes[325] = true;

        //Ready for death
        rightNotes[328] = true;
        rightNotes[329] = true;
        rightNotes[330] = true;
        leftNotes[331] = true;
        upNotes[331] = true;
        downNotes[331] = true;

        //There's no answer
        leftNotes[335] = true;
        upNotes[336] = true;
        rightNotes[338] = true;
        downNotes[339] = true;

        //Any time, any place
        leftNotes[341] = true;
        leftNotes[342] = true;
        leftNotes[343] = true;
        downNotes[343] = true;
        rightNotes[344] = true;
        rightNotes[345] = true;
        rightNotes[346] = true;
        upNotes[346] = true;

        //(Dun Duun, Don Doon)
        rightNotes[348] = true;
        upNotes[351] = true;
        downNotes[354] = true;
        leftNotes[357] = true;
        rightNotes[360] = true;
        upNotes[363] = true;
        downNotes[366] = true;
        leftNotes[368] = true;
        rightNotes[370] = true;
        upNotes[372] = true;
        downNotes[374] = true;
        leftNotes[376] = true;
        rightNotes[377] = true;
        upNotes[379] = true;
        downNotes[380] = true;
        leftNotes[381] = true;
        rightNotes[381] = true;
        rightNotes[383] = true;
        leftNotes[383] = true;

        //Looking for my answer
        leftNotes[387] = true;
        rightNotes[388] = true;
        upNotes[389] = true;
        downNotes[390] = true;
        leftNotes[392] = true;
        rightNotes[394] = true;

        //Looking for my dreams
        rightNotes[398] = true;
        downNotes[399] = true;
        upNotes[400] = true;
        leftNotes[401] = true;
        rightNotes[402] = true;
        leftNotes[404] = true;

        //Starting over
        upNotes[408] = true;
        leftNotes[408] = true;
        rightNotes[411] = true;
        downNotes[411] = true;
        downNotes[416] = true;
        rightNotes[416] = true;
        leftNotes[418] = true;
        upNotes[418] = true;

        //Looking for my answer
        leftNotes[428] = true;
        rightNotes[429] = true;
        upNotes[430] = true;
        downNotes[431] = true;
        leftNotes[432] = true;
        rightNotes[434] = true;

        //Looking for my future
        rightNotes[437] = true;
        upNotes[438] = true;
        downNotes[439] = true;
        leftNotes[440] = true;
        rightNotes[442] = true;
        leftNotes[444] = true;

        //Hey I got to watch I know
        upNotes[446] = true;
        downNotes[448] = true;
        upNotes[450] = true;
        downNotes[452] = true;
        leftNotes[454] = true;
        rightNotes[456] = true;
        rightNotes[458] = true;

        //Step by step
        leftNotes[462] = true;
        rightNotes[463] = true;
        leftNotes[463] = true;
        leftNotes[464] = true;

        //Hop through mountains
        upNotes[466] = true;
        downNotes[467] = true;
        upNotes[468] = true;
        upNotes[469] = true;

        //Step by step
        rightNotes[470] = true;
        leftNotes[471] = true;
        rightNotes[471] = true;
        rightNotes[472] = true;

        //Hop through oceans
        upNotes[473] = true;
        downNotes[474] = true;
        upNotes[475] = true;
        upNotes[476] = true;

        //Step by step
        leftNotes[477] = true;
        rightNotes[478] = true;
        leftNotes[478] = true;
        leftNotes[479] = true;

        //Hop forever
        upNotes[480] = true;
        downNotes[481] = true;
        upNotes[482] = true;
        upNotes[483] = true;

        //Going my way
        leftNotes[484] = true;
        leftNotes[486] = true;
        rightNotes[488] = true;
        rightNotes[489] = true;
        upNotes[489] = true;
        downNotes[489] = true;

        //Jumping down
        upNotes[491] = true;
        upNotes[492] = true;
        downNotes[493] = true;
        upNotes[493] = true;

        //Hop the mountains
        leftNotes[495] = true;
        rightNotes[496] = true;
        leftNotes[497] = true;
        leftNotes[498] = true;

        //Jumping down
        upNotes[498] = true;
        upNotes[499] = true;
        downNotes[500] = true;
        upNotes[500] = true;

        //Hop the oceans
        rightNotes[501] = true;
        leftNotes[502] = true;
        rightNotes[503] = true;
        rightNotes[504] = true;

        //Jumping down
        upNotes[505] = true;
        upNotes[506] = true;
        downNotes[507] = true;
        upNotes[507] = true;

        //Hop forever
        leftNotes[508] = true;
        rightNotes[509] = true;
        leftNotes[510] = true;

        //I can guess everything
        leftNotes[510] = true;
        leftNotes[511] = true;
        leftNotes[512] = true;
        rightNotes[513] = true;
        leftNotes[513] = true;
        rightNotes[514] = true;
        leftNotes[514] = true;
        rightNotes[515] = true;
        leftNotes[515] = true;


    }

    void Start ()
    {
        AudioSource audio = GetComponent<AudioSource>();
        audio.PlayDelayed(4.0f);
    }

    void FixedUpdate ()
    {
        timeStep += Time.deltaTime;
        if (timeStep > 0.295f && beat < 549)
        {
            scoreText.text = "Score: " + score.ToString();
            //beatText.text = "Beat: " + (beat - 15).ToString();

            if (leftNotes[beat] == true)
            {
                Instantiate(left, new Vector3(-2.25f, -6, 0), Quaternion.identity);
            }
            if (downNotes[beat] == true)
            {
                Instantiate(down, new Vector3(-0.75f, -6, 0), Quaternion.identity);
            }
            if (upNotes[beat] == true)
            {
                Instantiate(up, new Vector3(0.75f, -6, 0), Quaternion.identity);
            }
            if (rightNotes[beat] == true)
            {
                Instantiate(right, new Vector3(2.25f, -6, 0), Quaternion.identity);
            }
            timeStep = 0.0f;
            beat += 1;
        }
    }
}
