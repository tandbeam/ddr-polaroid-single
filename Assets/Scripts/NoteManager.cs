﻿using UnityEngine;
using System.Collections;

public class NoteManager : MonoBehaviour {
    private string input;

    void Start()
    {
        Destroy(gameObject, 6.0f);
        if (gameObject.name == "left(Clone)")
        {
            input = "left";
        }
        else if (gameObject.name == "down(Clone)")
        {
            input = "down";
        }
        else if (gameObject.name == "up(Clone)")
        {
            input = "up";
        }
        else
        {
            input = "right";
        }
    }

    void Update()
    {
        transform.Translate(Vector3.up * 2 * Time.deltaTime);
        if (Input.GetKeyDown(input))
        {
            if (transform.position.y >= 2.60f && transform.position.y <= 3.40f)
            {
                if (transform.position.y >= 2.95f && transform.position.y <= 3.05f)
                {
                    GameManager.score += 5;
                    GameManagerEasy.score += 5;
                    GameManagerHard.score += 5;
                }
                GameManager.score += 5;
                GameManagerEasy.score += 5;
                GameManagerHard.score += 5;
                Destroy(this.gameObject);
            }
        }
    }
}
